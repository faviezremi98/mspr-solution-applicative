-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 30 avr. 2019 à 12:32
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `open_inno`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

DROP TABLE IF EXISTS `adresse`;
CREATE TABLE IF NOT EXISTS `adresse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_id_app` int(11) NOT NULL,
  `numero_rue` int(11) NOT NULL,
  `rue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_postal` int(11) NOT NULL,
  `pays` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `adresse`
--

INSERT INTO `adresse` (`id`, `fk_id_app`, `numero_rue`, `rue`, `code_postal`, `pays`) VALUES
(9, 31, 0, 'd', 0, 'd');

-- --------------------------------------------------------

--
-- Structure de la table `appartement`
--

DROP TABLE IF EXISTS `appartement`;
CREATE TABLE IF NOT EXISTS `appartement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_id_usr` int(11) NOT NULL,
  `app_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_id_details` int(11) NOT NULL,
  `statut_app` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_appart` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_id_locataire` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `appartement`
--

INSERT INTO `appartement` (`id`, `fk_id_usr`, `app_name`, `fk_id_details`, `statut_app`, `type_appart`, `fk_id_locataire`) VALUES
(31, 5, 'd', 1, '1', 'Saisonnier', 3);

-- --------------------------------------------------------

--
-- Structure de la table `detailsapp`
--

DROP TABLE IF EXISTS `detailsapp`;
CREATE TABLE IF NOT EXISTS `detailsapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_id_app` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `detailsapp`
--

INSERT INTO `detailsapp` (`id`, `fk_id_app`) VALUES
(1, 23),
(2, 24),
(3, 25),
(4, 26),
(5, 27),
(6, 28),
(7, 29),
(8, 30),
(9, 31);

-- --------------------------------------------------------

--
-- Structure de la table `livret_accueil`
--

DROP TABLE IF EXISTS `livret_accueil`;
CREATE TABLE IF NOT EXISTS `livret_accueil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `loyer`
--

DROP TABLE IF EXISTS `loyer`;
CREATE TABLE IF NOT EXISTS `loyer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_pay` date NOT NULL,
  `montant` int(11) NOT NULL,
  `id_usr` int(11) NOT NULL,
  `id_loc` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20190305142728'),
('20190305143049'),
('20190305143420'),
('20190305144035'),
('20190305161234'),
('20190305164415'),
('20190328153448'),
('20190328174026'),
('20190417162816'),
('20190420094651'),
('20190420154844'),
('20190421100931'),
('20190421101124'),
('20190422170804'),
('20190423090406'),
('20190423130348'),
('20190423131035'),
('20190423133722'),
('20190423151851');

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valeur_note` int(11) NOT NULL,
  `fk_id_usr` int(11) NOT NULL,
  `fk_id_sejour` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `objet`
--

DROP TABLE IF EXISTS `objet`;
CREATE TABLE IF NOT EXISTS `objet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_obj` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etat_obj` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_obj_link` int(11) NOT NULL,
  `id_piece_link` int(11) NOT NULL,
  `fk_id_app` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

DROP TABLE IF EXISTS `photos`;
CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path_photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_id_app` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `photos`
--

INSERT INTO `photos` (`id`, `path_photo`, `fk_id_app`) VALUES
(1, 'c:/wamp64/1', 13),
(2, 'c:/wamp64/12e733a4bec3a06962cf4bc7d9b37b3c3a5fbf307.jpeg', 14),
(3, 'c:/wamp64/1af86ffd514d9f8f2d37b1858414981658af98a42.jpeg', 15),
(4, 'c:/wamp64/1c0c9c02d5c1e83d976a2ec6d24e7cc78eea6790f.jpeg', 16),
(5, 'c:/wamp64/11d6d2e5634ba16ea1448202ce2659999753ef3e9.jpeg', 17),
(6, 'c:/wamp64/188afd8e8709d68d8dcba545f92726ae31a7e22fb.jpeg', 18),
(7, 'c:/wamp64/1ef00ababb62d895a7823184e301c2db9489e9f65.jpeg', 19),
(8, 'c:/wamp64/1f3b0122edbe9818da2cda092d07e0a9541476755.png', 20),
(9, 'c:/wamp64/176587c5ef1ac019cddfa3885f88282c0034762c9.png', 21),
(10, 'c:/wamp64/167a5dbd9e1d84a67c4051f32bd223984359df9ef.png', 21),
(11, 'c:/wamp64/104aea0d72c529dea922470b859f110ccd76af71d.png', 22),
(12, 'c:/wamp64/1b1e10e5c24635defa16ce6a29477b92964007969.jpeg', 23),
(13, 'c:/wamp64/150e6b3f635e8f064871215cfb46cd51ce59e58eb.png', 24),
(14, 'c:/wamp64/3293a4d3464c7d66e6730113a9a50e0c56a064fa4.jpeg', 25),
(15, 'c:/wamp64/334f83ce2a390800779a236b93dbbd97d7323c80d.jpeg', 25),
(16, 'c:/wamp64/3d9c10db3d664298c6017d48b9a288e5816cfad54.jpeg', 25),
(17, 'c:/wamp64/33c15f0d8b802d9ad225959b3cffb75dd2a51110a.jpeg', 25),
(18, 'c:/wamp64/3fc613c0fddee6a5c91c56613e35119311c97e0a9.jpeg', 25),
(19, 'c:/wamp64/34cea8f1d0a2de218ded3ada9ce7bcbe9b97eb7e2.jpeg', 25),
(20, 'c:/wamp64/3d479d6b12429d2024f2155e18ac182a0bc8ece41.jpeg', 25),
(21, 'c:/wamp64/37a3d767ffa21623d222e08272dd7d0f5e64e050a.jpeg', 25),
(22, 'c:/wamp64/366f56d406fcb6676e2bbec24f76f5950097a8917.jpeg', 26),
(23, 'c:/wamp64/358d50b4c7f1e5ff7b4442d564e66f4def3741f78.jpeg', 27),
(24, 'c:/wamp64/3fb6a6c30a7613af44cc111c736441ddbc9eeb601.jpeg', 28),
(25, 'c:/wamp64/372b44ff5b6ba84f8c44e95e97ed6110c1aa0dc63.jpeg', 29),
(26, 'c:/wamp64/37bedea6f031840926d0d615d9ccacc741adfaccb.jpeg', 30),
(27, 'c:/wamp64/3f805efc1b7b4ab8b13e88d0afabc06b857bac9c4.jpeg', 25),
(28, 'c:/wamp64/325bbc60a603e0a77f86713156e5f7e8dee3ffb20.jpeg', 26),
(29, 'c:/wamp64/332e07fd35d79d522062626649e50c6b30f1e2599.jpeg', 31);

-- --------------------------------------------------------

--
-- Structure de la table `photos_placards`
--

DROP TABLE IF EXISTS `photos_placards`;
CREATE TABLE IF NOT EXISTS `photos_placards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path_photos_placards` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_id_placard` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `piece`
--

DROP TABLE IF EXISTS `piece`;
CREATE TABLE IF NOT EXISTS `piece` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_piece` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_id_usr` int(11) NOT NULL,
  `fk_id_app` int(11) NOT NULL,
  `type_piece` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sejour`
--

DROP TABLE IF EXISTS `sejour`;
CREATE TABLE IF NOT EXISTS `sejour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_id_app` int(11) NOT NULL,
  `fk_id_usr` int(11) NOT NULL,
  `fk_id_loc` int(11) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ticketincident`
--

DROP TABLE IF EXISTS `ticketincident`;
CREATE TABLE IF NOT EXISTS `ticketincident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usr` int(11) NOT NULL,
  `txt_incident` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statut_ticket` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activated` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `login`, `email`, `password`, `first_name`, `last_name`, `activated`, `token`, `type_user`, `phone_number`) VALUES
(1, 'DrHelios', 'romain.rebecchi@gmail.com', '$2y$13$c3ICGZvRdHUTBez1Gp5ZMOevn2yfvePyqsoDohg96J641Hvk3ZZgG', 'Romain', 'Rebecchi', 1, '$2y$13$LDnIbLRTri7PqM9BlzoLnOTTWiyOA/IyXzCZuyVNZ21wbRt27v2Ty', '', ''),
(2, 'remi', 'faviez.remi2@outlook.fr', '$2y$13$Sm48iYykpcPr0b9zUMOT/exP5CNcAI7a/J85JIKTXktLjp84qH.AK', 'remi', 'remi', 0, '$2y$13$XHBSE6mg86V2o6VMlLU6h.yntFMgUprlClipKMDebBnIBTRWJIBQe', '', ''),
(3, 'a', 'faviez.remi@outlook.fr', '$2y$13$AhM3tK2Qq5VHY9yAhhLmuurSpeAYuGx7dxTiPkcNS0.sOcAId8X.a', 'a', 'a', 1, '$2y$13$uCNKk/6lBy0Th6BfAAalR.tdNorSw2PzqVU0WtAFCzZKSVuhVQFCu', 'Locataire', '0789585824'),
(4, 'faviez.remi@outlook.fr', 'faviez2.remi@outlook.fr', '$2y$13$RruhZWzJSGE4WzO4vXdjgOfQNl/a.A02QTNJpbWXoA.HV.oyEyrPa', 'Rémi', 'Rémi', 0, '$2y$13$p1pUtCI/28GlCVcDBV/HB.J384TXscOdUIMo9QW5eOZ6yJg5Cm6S2', 'Locataire', '0789585824'),
(5, 'B', 'faviez.remi3@outlook.fr', '$2y$13$mhd5uJkmIMkHfb4mqlCCZeT6g65FUfxawEsmq/eu43Z8YgEaz6I0K', 'B', 'B', 1, '$2y$13$TaZM4EzOju6qcHinON49dO6gtvJplqw4Nv44tSO8MNW51I.0ML5cq', 'Locataire', '0789585824');

-- --------------------------------------------------------

--
-- Structure de la table `usr_xapp`
--

DROP TABLE IF EXISTS `usr_xapp`;
CREATE TABLE IF NOT EXISTS `usr_xapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usr` int(11) NOT NULL,
  `id_app` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
