<?php

namespace App\Repository;

use App\Entity\ResultsQuestionnaires;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ResultsQuestionnaires|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResultsQuestionnaires|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResultsQuestionnaires[]    findAll()
 * @method ResultsQuestionnaires[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResultsQuestionnairesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ResultsQuestionnaires::class);
    }

    // /**
    //  * @return ResultsQuestionnaires[] Returns an array of ResultsQuestionnaires objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ResultsQuestionnaires
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
