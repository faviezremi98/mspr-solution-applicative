<?php

namespace App\Repository;

use App\Entity\Histo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Histo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Histo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Histo[]    findAll()
 * @method Histo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Histo::class);
    }

    // /**
    //  * @return Histo[] Returns an array of Histo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Histo
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
