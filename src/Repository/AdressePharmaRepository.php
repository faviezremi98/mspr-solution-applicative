<?php

namespace App\Repository;

use App\Entity\AdressePharma;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AdressePharma|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdressePharma|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdressePharma[]    findAll()
 * @method AdressePharma[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdressePharmaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AdressePharma::class);
    }

    // /**
    //  * @return AdressePharma[] Returns an array of AdressePharma objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdressePharma
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
