<?php
/**
 * Created by PhpStorm.
 * User: favie
 * Date: 05/03/2019
 * Time: 14:19
 */
namespace App\Manager ;

use App\Entity\Photos;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class AppartManager {

    private $em;
    private $obManager;

    function __construct(EntityManagerInterface $em, ObjectManager $obManager) {
        $this->em = $em;
        $this->obManager = $obManager;
    }

    public function getAllApparts($userId): ?array
    {
        $records = $this->em->getRepository("App\Entity\Appartement")->findBy(['fk_id_usr' => $userId]);

        return $records;
    }

    public function getAllAppartsByType($userId, $criteria)
    {
        $records = $this->em->getRepository("App\Entity\Appartement")->findBy(['fk_id_usr' => $userId, 'type_appart' => $criteria]);

        return $records;

    }

    public function addPhotos($files, $userId, $appart)
    {
        foreach($files as $file)
        {
            $photos = new Photos();

            $path = sha1(uniqid(mt_rand(), true)).'.'.$file->guessExtension();
            $file->move("c:/wamp64/".$userId , $path); //mettre en chemin relatif
            $photos->setPathPhoto("c:/wamp64/".$userId."/".$path);
            $photos->setFkIdAppart($appart->getId());
            $this->obManager->persist($photos);
            $this->obManager->flush();
            $this->obManager->refresh($photos);

            unset($file);
        }
    }
}