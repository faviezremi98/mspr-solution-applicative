<?php

namespace App\Form;

use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;

class EnqueteSatisfactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nb_questions', NumberType::class, [
                "label" => "Nombre de questions"
            ])
            ->addEventListener(
            FormEvents::POST_SUBMIT,
            function(FormEvents $event)
            {
                $data = $event->getData();
                $form = $event->getForm();

                var_dump($data) ; die();

                for($i = 0; $i < $data ; $i++){
                    $form->getParent()->add('question', TextType::class, [
                        'label' => "Question :"
                    ])
                        ->add('nb_reponses', NumberType::class, [
                            "label" => "Nombre de réponses"
                        ])
                    ;
                }

            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
