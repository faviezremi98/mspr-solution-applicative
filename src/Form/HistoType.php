<?php

namespace App\Form;

use App\Entity\Histo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HistoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $pharmacie = $options['data'];

        $builder
            ->add('nom_produit')
            ->add('prix_produit')
            ->add('quantity')
            ->add('Pharmacie', ChoiceType::class, [
                "choices" => $pharmacie
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
//            'data_class' => Histo::class,
        ]);
    }
}
