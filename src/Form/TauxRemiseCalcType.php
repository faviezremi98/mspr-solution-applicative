<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TauxRemiseCalcType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('achat_brut', NumberType::class, [
                "label" => "Prix d'achat Brut",
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add(
                'achat_net', NumberType::class, [
                    "label" => "Prix d'achat Net",
                "attr" => [
                    "class" => "form-control"
                ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
