<?php

namespace App\Controller;

use App\Entity\AdressePharma;
use App\Entity\Histo;
use App\Entity\Pharmacie;
use App\Entity\Products;
use App\Entity\User;
use App\Form\AchatNetCalcType;
use App\Form\AddProductType;
use App\Form\CoeffCalcType;
use App\Form\HistoType;
use App\Form\TauxRemiseCalcType;
use App\Form\VenteNetCalcType;
use App\Manager\AppartManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DefaultController extends Controller
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/allcalculs", name="allCalculs")
     */
    public function homeAction(Request $request, EntityManagerInterface $em)
    {

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/taux-remise", name="taux-remise")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tauxRemiseAction()
    {
        $form = $this->createForm(TauxRemiseCalcType::class);

        return $this->render('Calc/TauxRemiseCalc.html.twig', [
            'controller_name' => 'DefaultController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/achat-net", name="achat-net")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function achatNetAction()
    {
        $form = $this->createForm(AchatNetCalcType::class);

        return $this->render('Calc/AchatNetCalc.html.twig', [
            'controller_name' => 'DefaultController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/vente-net", name="vente-net")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function venteNetAction()
    {
        $form = $this->createForm(VenteNetCalcType::class);

        return $this->render('Calc/VenteNetCalc.html.twig', [
            'controller_name' => 'DefaultController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/coeff", name="coeff")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function coeffAction()
    {
        $form = $this->createForm(CoeffCalcType::class);

        return $this->render('Calc/CoeffCalc.html.twig', [
            'controller_name' => 'DefaultController',
            'form' => $form->createView(),
        ]);
    }



    /**
     * @Route("/maps", name="maps")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function googleMapsAction()
    {
        $connected = @fsockopen("www.example.com", 80);
        //website, port  (try 80 or 443)
        if ($connected){
            $is_conn = true; //action when connected
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }

        if(!$is_conn){
            $all = $this->getDoctrine()->getRepository(Pharmacie::class)->findAll();
            $allPharma = array();
            foreach($all as $key => $element)
            {
                $fk_adresse = $element->getFkAdressePharma();
                $nom_pharma = $element->getNomPharma();
                $num_tel = $element->getNumTel();

                $adress_pharma = $this->getDoctrine()->getRepository(AdressePharma::class)->findBy(["id" => $fk_adresse]);
                $allPharma[$key]["adresse_pharma"]["rue"] = $adress_pharma[0]->getRue() ;
                $allPharma[$key]["adresse_pharma"]["num_rue"] = $adress_pharma[0]->getNumRue() ;
                $allPharma[$key]["num_tel"] = $num_tel;
                $allPharma[$key]["nom_pharma"] = $nom_pharma ;
                $allPharma[$key]["id"] = $element->getId();
            }
        } else {
            $allPharma = [];
        }

        return $this->render('Google maps/maps.html.twig', [
            'is_conn' => $is_conn,
            'controller_name' => 'DefaultController',
            'pharmacies' => $allPharma

        ]);
    }

    /**
     * @Route("/add-product", name="add-product")
     */
    public function addProduct(Request $request)
    {
        $product = new Products();

        $form = $this->createForm(AddProductType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {

            $product->setNom($form->get('Nom')->getData());
            $product->setPrix($form->get('Prix')->getData());

            $this->em->persist($product);
            $this->em->flush();
        }

        return $this->render('default/addProduct.html.twig', [
            'controller_name' => 'DefaultController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/do-histo", name="do-histo")
     */
    public function doHisto()
    {
        $allPharma = $this->getDoctrine()->getRepository(Pharmacie::class)->findAll();
        $pharma = new Pharmacie();

        $data = [];
        foreach($allPharma as $element)
        {
            $data[$element->getNomPharma()] = $element->getNomPharma();
        }

        $form = $this->createForm(HistoType::class, $pharma, [
            'data' => $data
        ]);

        return $this->render('default/histo.html.twig', [
            'controller_name' => 'DefaultController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("db-data-histo", name="data-histo")
     */
    public function flushDataHisto(Request $request)
    {
        $type = $request->query->get("type");
        $nom = $request->query->get("nom");
        $quantity = $request->query->get("quantity");
        $prix = $request->query->get("prix");
        $pharmacie = $request->query->get("Pharmacie");

        $idPharmacie = $this->getDoctrine()->getRepository(Pharmacie::class)->findOneBy(["nom_pharma" => $pharmacie]);

        $histo = new Histo();

            $histo->setNomProduit($nom);
            $histo->setPrixProduit(intval($prix));
            $histo->setQuantity(intval($quantity));
            $histo->setType($type);
            $histo->setFkIdPharma($idPharmacie->getId());

            $this->em->persist($histo);
            $this->em->flush($histo);

            return new JsonResponse("success");

    }

    /**
     * @Route("show-histo/{id}", name="show-histo", methods={"GET"})
     */
    public function showHisto(Pharmacie $pharmacie){

        $histo = $this->getDoctrine()->getRepository(Histo::class)->findBy(['fk_id_pharma' => $pharmacie->getId()]);

        return $this->render('default/show_histo.html.twig', [
            'controller_name' => 'DefaultController',
            'histo' => $histo
        ]);
    }


}
