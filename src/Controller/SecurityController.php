<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ForgotPasswordType;
use App\Form\RegistrationType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() and $form->isValid()){
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $token = $encoder->encodePassword($user, $user->getLogin());

            $user->setPassword($hash);
            $user->setActivated(0);
            $user->setToken($token);
            $roles = [];
            $roles[] = $form->get("roles")->getData() ;
            $user->setRoles($roles);
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute("security_login");
        }



        return $this->render('security/registration.html.twig', [
            'controller_name' => 'SecurityController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/", name="security_login")
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request, EntityManagerInterface $em): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('security/login.html.twig', ['error' => $error]);
    }

    /**
     * @Route("/activation", name="security_activation")
     */
    public function activation(Request $request, ObjectManager $manager)
    {
        $token = $request->query->get('token');

        $repo = $this->getDoctrine()->getRepository(User::class);

        $valid = $repo->findBy(
            ["Token" => $token ]
        );

        $myUser = $this->getDoctrine()->getRepository(User::class)->find($valid[0]);

        if($myUser->countUser($myUser) > 0)
        {
            $myUser->setActivated("1");
            $manager->persist($myUser);
            $manager->flush();
        }

        return $this->render('security/login.html.twig', ['error' => false,
            'activated' => "Votre compte a bien été activé !",
            'type_user' => $this->getUser()->getTypeUser()
        ]);

    }

    /**
     * @Route("/forgot_password", name="security_forgot.password")
     */
    public function forgotPassword(Request $request, \Swift_Mailer $mailer, EntityManagerInterface $em)
    {
        $user = new User();

        $form = $this->createForm(ForgotPasswordType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() and $form->isValid()){

            if(!empty($em->getRepository(User::class)->findBy(["Email" => $form->get("Email")->getData(), "LastName" => $form->get("LastName")->getData() ]))){
                $message = (new \Swift_Message('Hello Email'))
                    ->setFrom('faviezremi98@gmail.com')
                    ->setTo('faviez.remi@outlook.fr')
                    ->setSubject("Mot de passe oublié")
                    ->setBody(
                        $this->renderView(
                        // templates/emails/registration.html.twig
                            'emails/forgot_password.html.twig',
                            array('')
                        ),
                        'text/html'
                    );

                $mailer->send($message);
            }

            return $this->render('security/forgot_password.html.twig', [
                'form' => null
            ]);
        }


        return $this->render('security/forgot_password.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout() {

    }

}
