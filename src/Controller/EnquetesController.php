<?php

namespace App\Controller;

use App\Entity\Questionnaire;
use App\Entity\Questions;
use App\Entity\ResultsQuestionnaires;
use App\Form\EnqueteSatisfactionType;
use App\Kernel;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;


class EnquetesController extends AbstractController
{
    private $em;
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/index-enquetes", name="index-enquetes")
     */
    public function index()
    {
        return $this->render('enquetes/index.html.twig', [
            'controller_name' => 'EnquetesController',
        ]);
    }

    /**
     * @Route("/create-enquetes", name="create-enquetes")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createEnqueteAction()
    {
        $form = $this->createForm(EnqueteSatisfactionType::class);
        return $this->render('enquetes/create_enquete.html.twig', [
            'controller_name' => 'DefaultController',
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("creer-enquete-db", name="creer-enquete-db")
     */
    public function createEnqueteDb(Request $request){

        $nomQuestionnaire = $request->query->get("nom_questionnaire");

        $check = $this->getDoctrine()->getRepository(Questionnaire::class)->findOneBy(['nom_questionnaire' => $nomQuestionnaire]);

         if($check) {
            return new JsonResponse( array('error' => "Un formulaire avec ce nom existe déjà"));
         }

        $questionnaire = new Questionnaire();
        $questionnaire->setNomQuestionnaire($nomQuestionnaire);

        $this->em->persist($questionnaire);
        $this->em->flush();

        $id = $questionnaire->getId();

        $nbQuestions = $request->query->get("nb_questions");

        for($i = 0; $i < $nbQuestions ; $i ++){
            ${"questionObject". $i} = new Questions();
            ${"question". $i} = $request->query->get("question_".$i);
            ${"type". $i} = $request->query->get("type".$i);

            ${"questionObject". $i}->setContent( ${"question". $i});

            ${"questionObject". $i}->setFkIdQuestionnaire($id);
            if(${"type". $i} == "on"){
                ${"questionObject". $i}->setType(${"type". $i});
            } else {
                ${"questionObject". $i}->setType("off");
            }

            $this->em->persist(${"questionObject". $i});

        }


        $this->em->flush();

        return new JsonResponse("ok");

    }

    /**
     * @Route("show-enquetes", name="show-enquetes")
     */
    public function showEnquetes()
    {
        $allQuestionnaire = $this->getDoctrine()->getRepository(Questionnaire::class)->findAll();

        foreach($allQuestionnaire as $element)
        {
            $id = $element->getId();
            $questions = $this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(["fk_id_questionnaire" => $id, 'type' => "off"]);

            $count = count($questions);

            $questionsAgree = $this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(["fk_id_questionnaire" => $id, "result" => "agree", 'type' => "off"]);
            $countAgree = count($questionsAgree);

            $questionsMedium = $this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(["fk_id_questionnaire" => $id, "result" => "medium", 'type' => "off"]);
            $countMedium = count($questionsMedium);

            $questionsDisagree = $this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(["fk_id_questionnaire" => $id, "result" => "disagree", 'type' => "off"]);
            $countDisagree = count($questionsDisagree);

            if($count != 0){
                $stat['_'.$element->getNomQuestionnaire()]["agree"] = number_format($countAgree / $count * 100, 2);
                $stat['_'.$element->getNomQuestionnaire()]["medium"] = number_format($countMedium / $count * 100,2);
                $stat['_'.$element->getNomQuestionnaire()]["disagree"] = number_format($countDisagree / $count * 100,2);
            } else {
                $stat['_'.$element->getNomQuestionnaire()]["agree"] = 0;
                $stat['_'.$element->getNomQuestionnaire()]["medium"] = 0;
                $stat['_'.$element->getNomQuestionnaire()]["disagree"] = 0;
            }

        }

        return $this->render('enquetes/show_enquete.html.twig', [
            'controller_name' => 'DefaultController',
            'allQuestionnaire' => $allQuestionnaire,
            'stats' => $stat
        ]);
    }

    /**
     * @Route("enquetes/{id}", name="enquetes")
     */
    public function enqueteAction(Questionnaire $questionnaire)
    {
        $questions = $this->getDoctrine()->getRepository(Questions::class)->findBy(["fk_id_questionnaire" => $questionnaire->getId()]);

        return $this->render('enquetes/enquetes.html.twig', [
            'controller_name' => 'DefaultController',
            'questionnaire_name' => $questionnaire->getNomQuestionnaire(),
            'questions' => $questions
        ]);
    }

    /**
     * @Route("register-enquete", name="register-enquete")
     */
    public function registerEnquete()
    {
        $idQuestionnaire = $this->getDoctrine()->getRepository(Questionnaire::class)->findOneBy(["nom_questionnaire" => $_POST["name"]])->getId();

        foreach($_POST as $key => $element){
            var_dump($key);
            if($key !== "name" && $key !== "type")
            {
                $result = new ResultsQuestionnaires();

                $result->setFkIdQuestion(intval($key));
                $result->setFkIdQuestionnaire($idQuestionnaire);
                $result->setResult($element);
                $result->setType($_POST["type"]);
                $this->em->persist($result);


            }

        }



        $this->em->flush();

        return $this->redirectToRoute("show-enquetes");
    }

    /**
     * @Route("exportAllEnquetes", name="exportAllEnquetes")
     */
    public function exportAllEnquetes(KernelInterface $kernel)
    {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('D1', 'Résultats des enquêtes de satisfaction ! ');
        $sheet->setTitle("Enquete_satisfaction_stats");

        $allEnquetes = $this->getDoctrine()->getRepository(Questionnaire::class)->findAll();

        $count = 1;

        foreach($allEnquetes as $element)
        {
            $count ++ ;
            $allQuestionsRadio = $this->getDoctrine()->getRepository(Questions::class)->findBy(['fk_id_questionnaire' => $element->getId(), 'type' => "off"]);
            $allQuestionsRiche = $this->getDoctrine()->getRepository(Questions::class)->findBy(['fk_id_questionnaire' => $element->getId(), 'type' => "on"]);
            $sheet->setCellValue('A'.$count, $element->getNomQuestionnaire());

            if(!empty($allQuestionsRadio))
            {
                foreach ($allQuestionsRadio as $question)
                {

                    $count++;

                    $resultAgree = count($this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(['fk_id_question' => $question->getId(), 'fk_id_questionnaire' => $element->getId(), 'result' => 'agree']));
                    $resultmedium = count($this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(['fk_id_question' => $question->getId(), 'fk_id_questionnaire' => $element->getId(), 'result' => 'medium']));
                    $resultdisagree = count($this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(['fk_id_question' => $question->getId(), 'fk_id_questionnaire' => $element->getId(), 'result' => 'disagree']));

                    $nbQuestionsRadio = count($this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(['fk_id_questionnaire' => $element->getId(), 'type' => "off"]));

                    if($nbQuestionsRadio != 0)
                    {
                        $statAgree = number_format($resultAgree / $nbQuestionsRadio * 100, 2);
                        $statmedium = number_format($resultmedium / $nbQuestionsRadio * 100, 2);
                        $statdisagree = number_format($resultdisagree / $nbQuestionsRadio * 100, 2);

                        $sheet->setCellValue('A'.$count, utf8_encode($question->getContent()));

                        $sheet->setCellValue('B'.$count, "Satisfaisant : ".$statAgree."%");
                        $count++;
                        $sheet->setCellValue('B'.$count, "Peu Satisfaisant : ".$statmedium."%");
                        $count++;
                        $sheet->setCellValue('B'.$count, "Non Satisfaisant : ".$statdisagree."%");
                    }


                }
            }

            if(!empty($allQuestionsRiche))
            {
                foreach ($allQuestionsRiche as $question)
                {
                    $count++;
                    $sheet->setCellValue('A'.$count, $question->getContent());

                    $result = $this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findOneBy(['fk_id_question' => $question->getId(), 'fk_id_questionnaire' => $element->getId()]);

                    $sheet->setCellValue('B'.$count, $result->getResult());

                }
            }
        }

        $writer = new Xls($spreadsheet);

        $response = new Response();

        $response->headers->set("Content-type", " application/vnd.ms-excel");
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-Disposition', ' attachment; filename="my_first_exce.xls');

        $response->sendHeaders();
        $response->setContent($writer->save("php://output"));

        return $response;

    }


}
