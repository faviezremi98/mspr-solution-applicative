<?php

namespace App\Controller;

use App\Entity\Histo;
use App\Entity\Products;
use App\Entity\Questionnaire;
use App\Entity\Questions;
use App\Entity\ResultsQuestionnaires;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class ApiController
 * @Route("/api")
 * @package App\Controller
 */
class ApiController extends AbstractController
{

    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em =  $em;
    }

    /**
     * @Route("/authenticate", name="authenticate")
     */
    public function index(Request $request)
    {
        $error = null ;

        $email = $request->query->get("email");
        $password = $request->query->get("password");

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(["Email" => $email, 'Password' => $password]);

        if($user == null)
        {
            $error = "Impossible de vous connecter";
        }

        $email = $user->getEmail() ;

        return new JsonResponse(
             array(
                 'email' => $email
             )
        );
    }

    /**
     * @Route("/test", name="test")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function test()
    {
        return $this->render('api/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/allEnquetes", name="allEnquetes")
     * @return JsonResponse
     */
    public function allEnquetes()
    {
        $response = [];
        $allQuestionnaire = $this->getDoctrine()->getRepository(Questionnaire::class)->findAll();



        foreach ($allQuestionnaire as $key => $element){

            $id = $element->getId();
            $questions = $this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(["fk_id_questionnaire" => $id]);

            $count = count($questions);

            $questionsAgree = $this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(["fk_id_questionnaire" => $id, "result" => "agree"]);
            $countAgree = count($questionsAgree);

            $questionsMedium = $this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(["fk_id_questionnaire" => $id, "result" => "medium"]);
            $countMedium = count($questionsMedium);

            $questionsDisagree = $this->getDoctrine()->getRepository(ResultsQuestionnaires::class)->findBy(["fk_id_questionnaire" => $id, "result" => "disagree"]);
            $countDisagree = count($questionsDisagree);

            if($count != 0){
                $stat['_'.$element->getNomQuestionnaire()]["agree"] = $countAgree / $count * 100;
                $stat['_'.$element->getNomQuestionnaire()]["medium"] = $countMedium / $count * 100;
                $stat['_'.$element->getNomQuestionnaire()]["disagree"] = $countDisagree / $count * 100;
            } else {
                $stat['_'.$element->getNomQuestionnaire()]["agree"] = 0;
                $stat['_'.$element->getNomQuestionnaire()]["medium"] = 0;
                $stat['_'.$element->getNomQuestionnaire()]["disagree"] = 0;
            }

            $response[$key]["questionnaire"] = $element;
            $response[$key]["percent"] = $stat;
            $response[$key]["id"] = $element->getID();
            $response[$key]["name"] = $element->getNomQuestionnaire();
        }

        return new JsonResponse($response);
    }

     /**
      * @Route("/allQuestions", name="allQuestions")
      */
    public function getQuestionBySondage(Request $request)
    {
        $idQuestionnaire = $request->query->get("idQuestionnaire");

        $questionnaire = $this->getDoctrine()->getRepository(Questionnaire::class)->findOneBy(['id' => $idQuestionnaire]);

        $questions = $this->getDoctrine()->getRepository(Questions::class)->findBy(["fk_id_questionnaire" => $questionnaire->getId()]);

        $response = [];

        foreach ($questions as $key => $element){
            $response[$key]["question"] = $element->getContent();
            $response[$key]["type"] = $element->getType();
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/allProducts", name="allProducts")
     */
    public function getAllProducts(){

        $allProducts = $this->getDoctrine()->getRepository(Products::class)->findAll();

        $response = [];

        foreach($allProducts as $key => $element){
            $response[$key]["productNom"] = $element->getNom();
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/getHisto", name="getHisto")
     */
    public function getHisto(){

        $histo = $this->getDoctrine()->getRepository(Histo::class)->findAll();

        $response = [];

        foreach($histo as $key => $element)
        {
            $response[$key]["type"] = $element->getType();
            $response[$key]["nom_produit"] = $element->getNomProduit();
            $response[$key]["prix_produit"] = $element->getPrixProduit();
            $response[$key]["quantity"] = $element->getQuantity();
        }

        return new JsonResponse($response);

    }




}
