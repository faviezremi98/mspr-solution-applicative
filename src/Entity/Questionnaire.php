<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionnaireRepository")
 */
class Questionnaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_questionnaire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomQuestionnaire(): ?string
    {
        return $this->nom_questionnaire;
    }

    public function setNomQuestionnaire(string $nom_questionnaire): self
    {
        $this->nom_questionnaire = $nom_questionnaire;

        return $this;
    }
}
