<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PharmacieRepository")
 */
class Pharmacie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_pharma;

    /**
     * @ORM\Column(type="integer")
     */
    private $fk_adresse_pharma;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $num_tel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPharma(): ?string
    {
        return $this->nom_pharma;
    }

    public function setNomPharma(string $nom_pharma): self
    {
        $this->nom_pharma = $nom_pharma;

        return $this;
    }



    /**
     * @return mixed
     */
    public function getFkAdressePharma()
    {
        return $this->fk_adresse_pharma;
    }

    /**
     * @param mixed $fk_adresse_pharma
     */
    public function setFkAdressePharma($fk_adresse_pharma): void
    {
        $this->fk_adresse_pharma = $fk_adresse_pharma;
    }

    public function getNumTel(): ?string
    {
        return $this->num_tel;
    }

    public function setNumTel(string $num_tel): self
    {
        $this->num_tel = $num_tel;

        return $this;
    }
}
