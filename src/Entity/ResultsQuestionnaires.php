<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultsQuestionnairesRepository")
 */
class ResultsQuestionnaires
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $fk_id_question;

    /**
     * @ORM\Column(type="integer")
     */
    private $fk_id_questionnaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $result;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkIdQuestion(): ?int
    {
        return $this->fk_id_question;
    }

    public function setFkIdQuestion(int $fk_id_question): self
    {
        $this->fk_id_question = $fk_id_question;

        return $this;
    }

    public function getFkIdQuestionnaire(): ?int
    {
        return $this->fk_id_questionnaire;
    }

    public function setFkIdQuestionnaire(int $fk_id_questionnaire): self
    {
        $this->fk_id_questionnaire = $fk_id_questionnaire;

        return $this;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function setResult(string $result): self
    {
        $this->result = $result;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
