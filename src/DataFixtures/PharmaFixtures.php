<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Pharmacie;

class PharmaFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $pharma = new Pharmacie();
        $pharma->setNomPharma("Bellini");
        $pharma->setFkAdressePharma(1);

        $manager->persist($pharma);

        $manager->flush();
    }
}
