<?php

namespace App\DataFixtures;

use App\Entity\AdressePharma;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AdressePharmaFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $adrPharma = new AdressePharma();
        $adrPharma->setCodePostal(75000);
        $adrPharma->setNumRue(1);
        $adrPharma->setRue("Rue de ta mère");
        $adrPharma->setVille("Paris");

        $manager->flush();
    }
}
