<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
       $user = new User();
       $user->setPassword("test");
       $user->setEmail("test@test.fr");
       $user->setActivated(1);
       $user->setLogin("DMO");
       $user->setToken("token");
       $manager->persist($user);

        $manager->flush();
    }
}
