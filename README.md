# MSPR Solution applicative

* POUR : les DMO (délégués médicaux), ou visiteur médical (VM) ou représentant médical (au Canada).
* QUI SOUHAITENT : 
*                  V1. Application qui permet la présentation des produits aux pharmaciens et practiciens. Chacun d'eux auront une visions différentes
                   ainsi il leur faudra une présentation différente, un mode "practice" et un mode "pharma" sera a disposition permettant une presentation optimisé,
                    et une calculatrice sera intégré pour pouvoir faire des achats de masses reliés au stock disponible. Faire un devis en live sera donc disponible.
*                  V2. géolocalisation des pharmacies et on permet la visualisation des données des pharmacies disponibles, soit une gestion de stock par 
                   l'application, les services que proposent la pharmacie ainsi que les horaires, les gardes prévues.
*                  V3. Mis en place des formulaires et enquêtes de satisfactions. On propose un formulaire générique permettant à l'utilisateur final de créer ses propres 
                   formulaire permettant ainsi de viser différents public. Nivantis pourra ainsi créer des sondages quant à l'utilisation de l'application par les DMO,
                   mais aussi  par les practiciens et les pharmaciens. Chaque public pourra être touché.
              
* NOTRE PRODUIT EST : Une solution applicative cross-platform facilitant l'achat et la vente des produits par les commerciaux
* QUI : Nivantis, leur permettre une optimisation de leur travail pour augmenter leurs vente et ainsi se développer.
* A LA DIFFERENCE DE : Prise en main facile et synchronisation complète des applications en gardant un thème commun permettant à une personne de passer d'un environnement à un autre très rapidement, ce qui lui permet de ne pas être déstabilisé
* PERMET DE: Faire une vente en un éclair qui, pour les meilleurs commerciaux permettrait une vente en quelques minutes.

Pour lancer le Git : 

    * git clone https://gitlab.com/faviezremi98/mspr-solution-applicative/new/master ou ssh si on est enregistré
    * version pc on reste sur la branche master 
    * version mobile on fait git checkout mobile
    
PC :
    dans une commande line interface faire php bin/console server:run (/!\ PHP7.2 minimum) pour lancer le serveur